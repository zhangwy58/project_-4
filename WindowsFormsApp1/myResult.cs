﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public static class myResult
    {

        static class Myresult
        {
            // S0, K, r, T, sigma, trials, N
            public static double Myprice { get; set; }
            public static double Mydelta { get; set; }
            public static double Mygamma { get; set; }
            public static double Myvega { get; set; }
            public static double Mytheta { get; set; }
            public static double Myrho { get; set; }
            public static double MySE { get; set; }
            public static double S0 { get; set; }
            public static int trials { get; set; }
            public static int N { get; set; }

        }
    }
}
