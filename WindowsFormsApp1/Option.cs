﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
/// <summary>
///  This class is working as a option pricing method
/// </summary>
namespace WindowsFormsApp1
{
    public class Option
    {

        Simulator Simulator1 = new Simulator();
        RandomGenerator RG = new RandomGenerator();

        Stopwatch watch = new Stopwatch();

        public void scheduler()
        {
            Stopwatch watch = new Stopwatch();
            Summary sum1 = new Summary();

            watch.Start();
            sum1.Sum();
            watch.Stop();
            Program.increase(1);
            IO.myTime = watch.Elapsed.Hours.ToString() + ":" + watch.Elapsed.Minutes.ToString() + ":" + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        // This function is used to get the simulations and store it in sim
        public double[,] GetSimulations(double S0, double r, double T, double sigma,  int trials, int N, double[,] RandomNormal1, int numOfThread)
        {
            double[,] sim;
            sim = Simulator1.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            return sim;
        }
    }


    public class EuroOption : Option
    {

        // This function is used to calculate option price
        public double EUPrice(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            ControlVariate CV1 = new ControlVariate();

            if (useCV == true)
            {

                if (isAnti == true)
                {

                    if (isput == false)
                    {
                        double CV_P_Anti = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput=false, isAnti=true, isSE=false, numOfThread);

                        return CV_P_Anti;
                    }
                    else
                    {
                        double CV_P_Anti = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput=true, isAnti=true, isSE=false, numOfThread);

                        return CV_P_Anti;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        double CV_P = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput=false, isAnti=false, isSE=false, numOfThread);

                        return CV_P;
                    }
                    else
                    {
                        double CV_P = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput=true, isAnti=false, isSE=false, numOfThread);

                        return CV_P;
                    }

                }
                
            }
            else
            {
                if (isAnti == true)
                {
                    // Define variables

                    double sum1 = 0;
                    double sum2 = 0;

                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sims2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sum1 += Math.Max(sims[a, N] - K, 0);
                            sum2 += Math.Max(sims2[a, N] - K, 0);
                        }
                        else
                        {
                            sum1 += Math.Max(K - sims[a, N], 0);
                            sum2 += Math.Max(K - sims2[a, N], 0);
                        }
                    }
                    return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);
                }
                else
                {
                    // Define variables
                    double sum = 0;
                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sum += Math.Max(sims[a, N] - K, 0);
                        }
                        else
                        {
                            sum += Math.Max(K - sims[a, N], 0);
                        }
                    }
                    return (sum / trials) * Math.Exp(-r * T);
                }
            }
            
        }

    }
}
