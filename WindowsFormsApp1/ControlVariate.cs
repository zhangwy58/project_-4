﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WindowsFormsApp1
{
    class ControlVariate
    {

        public double CV(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, int numOfThread)
        {

            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;
            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];
            if (isAnti == true)
            {
                if (isput == false)
                {
                    // double[] sum_CT = new double[N];
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(St1 - K, 0) + beta1 * cv1) + (Math.Max(St2 - K, 0) + beta1 * cv2));
                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }
                    
                }
                else
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(K - St1, 0) + beta1 * cv1) + (Math.Max(K - St2, 0) + beta1 * cv2));
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }
                    
                }
            }
            else
            {
                if (isput == false)
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(St - K, 0) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }
                    
                }
                else
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(0, K - St) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }
                    
                }
            }

        }

    }
}
