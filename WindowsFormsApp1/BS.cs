﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class BS
    {
        public static double CumDensity(double z)
        {
            double p = 0.3275911;
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;

            int sign;
            if (z < 0.0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(z) / Math.Sqrt(2.0);
            double t = 1.0 / (1.0 + p * x);
            double erf = 1.0 - (((((a5 * t + a4) * t) + a3)
              * t + a2) * t + a1) * t * Math.Exp(-x * x);
            return 0.5 * (1.0 + sign * erf);
        }

        // Calculating d1 in Black Scholes Model.
        public static double BS_d1(double S0, double K, double r, double T, double sigma, double t)
        {

            double d1 = (Math.Log(S0 / K) + (r + (sigma * sigma) / 2) * (T-t)) / (sigma * Math.Sqrt(T-t));

            return d1;

        }

        // Calculating the delta value of call option by using Black Scholes Model
        public static double BS_Delta(double S0, double K, double r, double T, double sigma, double t, bool isput)
        {
            if (isput == false)
            {
                double BS_Call_Delta = CumDensity(BS_d1(S0, K, r, T, sigma, t));

                return BS_Call_Delta;
            }
            else
            {
                double BS_Put_Delta = CumDensity(BS_d1(S0, K, r, T, sigma, t)) - 1;

                return BS_Put_Delta;
            }
           
        }

    }

}
