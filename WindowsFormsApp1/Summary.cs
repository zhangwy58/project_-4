﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Summary
    {
        public void Sum()
        {
            // if all the inputs are correct, do the following calculation
            // Calculating the European call option price and greeks
            EuroOption opt1 = new EuroOption();
            Greeks greek1 = new Greeks();

            // Create a instance for RandomGenerator class
            RandomGenerator RG = new RandomGenerator();
            double[,] RandomNormal1 = RG.RandomNormal(IO.trials, IO.N, IO.numOfThread);
            double[,] RandomNormal2 = RG.NegtRandomNormal(IO.trials, IO.N, RandomNormal1, IO.numOfThread);

            // Calculating and outputing the european call option price
            IO.myPrice = opt1.EUPrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the delta value for European call option when user want to use antithetic method to reduce variance

            IO.myDelta = greek1.Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the gamma value for European call option when user want to use antithetic method to reduce variance
            IO.myGamma = greek1.Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the vega value for European call option when user want to use antithetic method to reduce variance
            IO.myVega = greek1.Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the theta value for European call option when user want to use antithetic method to reduce variance
            IO.myTheta = greek1.Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the rho value for European call option when user want to use antithetic method to reduce variance
            IO.myRho = greek1.Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the SE value for European call option when user want to use antithetic method to reduce variance
            IO.mySE = greek1.SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);

        }
    }
}
