﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This class is used to calculate the greeks and standard error
/// </summary>
namespace WindowsFormsApp1 
{
    class Greeks
    {
        Option opt1 = new Option();
        EuroOption opt2 = new EuroOption();


        // The function used to calculate the Delta when users do not want to use any method to reduece variance
        public double Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    } 
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {

                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            } 

        }

        // The function used to calculate the Gamma when users do not want to use any method to reduece variance
        public double Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;

                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
            

        }

        // The function used to calculate the Vega when users do not want to use any method to reduece variance
        public double Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }

        }

        // The function used to calculate the Theta when users do not want to use any method to reduece variance
        public double Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    // Define the variables for calculating the theta value
                    double dT = T * 0.001;
                    double Tm = T - dT;

                    // Using the new simulation to calculate the option price
                    double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                    // Calculate the theta value
                    double Theta = (Theta_p1 - Theta_p2) / dT;

                    return Theta;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }


        }

        // The function used to calculate the rho when users do not want to use any method to reduece variance 
        public double Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
        }

        // The function to calculate standard error value 
        public double SE(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;

                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(St1 - K, 0) + beta1 * cv1) + (Math.Max(St2 - K, 0) + beta1 * cv2));
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                    else
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(K - St1, 0) + beta1 * cv1) + (Math.Max(K - St2, 0) + beta1 * cv2));
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                }
                else
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;


                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(St - K, 0) + beta1 * cv;
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                    else
                    {

                        for (int i = 0; i < trials; i++)
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(0, K - St) + beta1 * cv;
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {

                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sims2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                    double EUPrice = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);


                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sums = sums + Math.Pow(((Math.Max(sims[a, N] - K, 0) * Math.Exp(-r * T) + Math.Max(sims2[a, N] - K, 0)) / 2 - EUPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow(((Math.Max(K - sims[a, N], 0) * Math.Exp(-r * T) + Math.Max(K - sims2[a, N], 0)) / 2 - EUPrice), 2);
                        }

                        // sums = sums + Math.Pow(((sims[a, N] * Math.Exp(-r * T) + sims2[a, N] * Math.Exp(-r * T)) / 2 - EUPrice), 2);
                    }

                    double SD = Math.Sqrt(sums / (trials - 1));

                    return SD / Math.Sqrt(trials);
                }
                else
                {
                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double EUPrice = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sums = sums + Math.Pow((Math.Max(sims[a, N] - K, 0) * Math.Exp(-r * T) - EUPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow((Math.Max(K - sims[a, N], 0) * Math.Exp(-r * T) - EUPrice), 2);
                        }

                    }

                    double SD = Math.Sqrt(sums / (trials - 1));
                    return SD / Math.Sqrt(trials);
                }
            }
        }
        
    }
}
