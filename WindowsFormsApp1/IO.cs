﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public static class IO
    {

        // Using user's inputs
        public static double S0 = 0.0;
        public static double K = 0.0;
        public static double r = 0.0;
        public static double T = 0.0;
        public static double sigma = 0.0;
        public static int N = 0;
        public static int trials = 0;
        public static int numOfThread = 1;
        public static bool isSE = false;
        public static bool isAnti = false;
        public static bool isput = false;
        public static bool useCV = false;


        // My final outputs variables
        public static double myPrice { get; set; }
        public static double myDelta { get; set; }
        public static double myGamma { get; set; }
        public static double myVega { get; set; }
        public static double myTheta { get; set; }
        public static double myRho { get; set; }
        public static double mySE { get; set; }
        public static string myTime { get; set; }



    }
}