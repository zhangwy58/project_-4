﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox_S = new System.Windows.Forms.TextBox();
            this.label_S = new System.Windows.Forms.Label();
            this.textBox_K = new System.Windows.Forms.TextBox();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.errorProvider_S = new System.Windows.Forms.ErrorProvider(this.components);
            this.label_K = new System.Windows.Forms.Label();
            this.errorProvider_K = new System.Windows.Forms.ErrorProvider(this.components);
            this.label_r = new System.Windows.Forms.Label();
            this.textBox_r = new System.Windows.Forms.TextBox();
            this.errorProvider_r = new System.Windows.Forms.ErrorProvider(this.components);
            this.label_T = new System.Windows.Forms.Label();
            this.textBox_T = new System.Windows.Forms.TextBox();
            this.errorProvider_T = new System.Windows.Forms.ErrorProvider(this.components);
            this.label_sigma = new System.Windows.Forms.Label();
            this.textBox_sigma = new System.Windows.Forms.TextBox();
            this.errorProvider_sigma = new System.Windows.Forms.ErrorProvider(this.components);
            this.textBox_N = new System.Windows.Forms.TextBox();
            this.label_N = new System.Windows.Forms.Label();
            this.errorProvider_N = new System.Windows.Forms.ErrorProvider(this.components);
            this.label_trials = new System.Windows.Forms.Label();
            this.textBox_trials = new System.Windows.Forms.TextBox();
            this.errorProvider_trials = new System.Windows.Forms.ErrorProvider(this.components);
            this.radioButton_EuropeanCall = new System.Windows.Forms.RadioButton();
            this.radioButton_EuropeanPut = new System.Windows.Forms.RadioButton();
            this.label_OptionResult = new System.Windows.Forms.Label();
            this.label_Delta = new System.Windows.Forms.Label();
            this.label_DeltaResult = new System.Windows.Forms.Label();
            this.label_Gamma = new System.Windows.Forms.Label();
            this.label_GammaResult = new System.Windows.Forms.Label();
            this.label_Vega = new System.Windows.Forms.Label();
            this.label_VegaResult = new System.Windows.Forms.Label();
            this.label_Theta = new System.Windows.Forms.Label();
            this.label_ThetaResult = new System.Windows.Forms.Label();
            this.label_Rho = new System.Windows.Forms.Label();
            this.label_RhoResult = new System.Windows.Forms.Label();
            this.label_SE = new System.Windows.Forms.Label();
            this.label_SEResult = new System.Windows.Forms.Label();
            this.checkBox_Antithetic = new System.Windows.Forms.CheckBox();
            this.label_VarianceReduction = new System.Windows.Forms.Label();
            this.checkBox_CV = new System.Windows.Forms.CheckBox();
            this.label_Time = new System.Windows.Forms.Label();
            this.label_TimeResult = new System.Windows.Forms.Label();
            this.checkBox_MT = new System.Windows.Forms.CheckBox();
            this.progressBar_Calculation = new System.Windows.Forms.ProgressBar();
            this.label_Cores = new System.Windows.Forms.Label();
            this.label_CoresResult = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.label_optionPrice = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_r)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_T)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_sigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_trials)).BeginInit();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_S
            // 
            this.textBox_S.Location = new System.Drawing.Point(262, 39);
            this.textBox_S.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_S.Name = "textBox_S";
            this.textBox_S.Size = new System.Drawing.Size(124, 23);
            this.textBox_S.TabIndex = 0;
            this.textBox_S.TextChanged += new System.EventHandler(this.textBox_S_TextChanged);
            // 
            // label_S
            // 
            this.label_S.AutoSize = true;
            this.label_S.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_S.Location = new System.Drawing.Point(81, 42);
            this.label_S.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_S.Name = "label_S";
            this.label_S.Size = new System.Drawing.Size(151, 16);
            this.label_S.TabIndex = 1;
            this.label_S.Text = "Underlying Price (S):";
            this.label_S.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBox_K
            // 
            this.textBox_K.Location = new System.Drawing.Point(262, 97);
            this.textBox_K.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_K.Name = "textBox_K";
            this.textBox_K.Size = new System.Drawing.Size(124, 23);
            this.textBox_K.TabIndex = 2;
            this.textBox_K.TextChanged += new System.EventHandler(this.textBox_K_TextChanged);
            // 
            // btn_calculate
            // 
            this.btn_calculate.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_calculate.Location = new System.Drawing.Point(653, 543);
            this.btn_calculate.Margin = new System.Windows.Forms.Padding(4);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(208, 107);
            this.btn_calculate.TabIndex = 3;
            this.btn_calculate.Text = "Calculate";
            this.btn_calculate.UseVisualStyleBackColor = true;
            this.btn_calculate.Click += new System.EventHandler(this.btn_calculate_Click);
            // 
            // errorProvider_S
            // 
            this.errorProvider_S.ContainerControl = this;
            // 
            // label_K
            // 
            this.label_K.AutoSize = true;
            this.label_K.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_K.Location = new System.Drawing.Point(119, 97);
            this.label_K.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_K.Name = "label_K";
            this.label_K.Size = new System.Drawing.Size(117, 16);
            this.label_K.TabIndex = 4;
            this.label_K.Text = "Strike Price (K):";
            // 
            // errorProvider_K
            // 
            this.errorProvider_K.ContainerControl = this;
            // 
            // label_r
            // 
            this.label_r.AutoSize = true;
            this.label_r.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_r.Location = new System.Drawing.Point(100, 157);
            this.label_r.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_r.Name = "label_r";
            this.label_r.Size = new System.Drawing.Size(135, 16);
            this.label_r.TabIndex = 5;
            this.label_r.Text = "Risk Free Rate (r):";
            // 
            // textBox_r
            // 
            this.textBox_r.Location = new System.Drawing.Point(262, 154);
            this.textBox_r.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_r.Name = "textBox_r";
            this.textBox_r.Size = new System.Drawing.Size(124, 23);
            this.textBox_r.TabIndex = 6;
            this.textBox_r.TextChanged += new System.EventHandler(this.textBox_r_TextChanged);
            // 
            // errorProvider_r
            // 
            this.errorProvider_r.ContainerControl = this;
            // 
            // label_T
            // 
            this.label_T.AutoSize = true;
            this.label_T.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_T.Location = new System.Drawing.Point(167, 214);
            this.label_T.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_T.Name = "label_T";
            this.label_T.Size = new System.Drawing.Size(74, 16);
            this.label_T.TabIndex = 7;
            this.label_T.Text = "Tenor (T):";
            // 
            // textBox_T
            // 
            this.textBox_T.Location = new System.Drawing.Point(262, 211);
            this.textBox_T.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_T.Name = "textBox_T";
            this.textBox_T.Size = new System.Drawing.Size(124, 23);
            this.textBox_T.TabIndex = 8;
            this.textBox_T.TextChanged += new System.EventHandler(this.textBox_T_TextChanged);
            // 
            // errorProvider_T
            // 
            this.errorProvider_T.ContainerControl = this;
            // 
            // label_sigma
            // 
            this.label_sigma.AutoSize = true;
            this.label_sigma.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sigma.Location = new System.Drawing.Point(110, 271);
            this.label_sigma.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_sigma.Name = "label_sigma";
            this.label_sigma.Size = new System.Drawing.Size(125, 16);
            this.label_sigma.TabIndex = 9;
            this.label_sigma.Text = "Volatility (sigma):";
            // 
            // textBox_sigma
            // 
            this.textBox_sigma.Location = new System.Drawing.Point(262, 269);
            this.textBox_sigma.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_sigma.Name = "textBox_sigma";
            this.textBox_sigma.Size = new System.Drawing.Size(124, 23);
            this.textBox_sigma.TabIndex = 10;
            this.textBox_sigma.TextChanged += new System.EventHandler(this.textBox_sigma_TextChanged);
            // 
            // errorProvider_sigma
            // 
            this.errorProvider_sigma.ContainerControl = this;
            // 
            // textBox_N
            // 
            this.textBox_N.Location = new System.Drawing.Point(262, 322);
            this.textBox_N.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_N.Name = "textBox_N";
            this.textBox_N.Size = new System.Drawing.Size(124, 23);
            this.textBox_N.TabIndex = 11;
            this.textBox_N.TextChanged += new System.EventHandler(this.textBox_N_TextChanged);
            // 
            // label_N
            // 
            this.label_N.AutoSize = true;
            this.label_N.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_N.Location = new System.Drawing.Point(165, 325);
            this.label_N.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_N.Name = "label_N";
            this.label_N.Size = new System.Drawing.Size(76, 16);
            this.label_N.TabIndex = 12;
            this.label_N.Text = "Steps (N):";
            // 
            // errorProvider_N
            // 
            this.errorProvider_N.ContainerControl = this;
            // 
            // label_trials
            // 
            this.label_trials.AutoSize = true;
            this.label_trials.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_trials.Location = new System.Drawing.Point(197, 374);
            this.label_trials.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_trials.Name = "label_trials";
            this.label_trials.Size = new System.Drawing.Size(49, 16);
            this.label_trials.TabIndex = 13;
            this.label_trials.Text = "Trials:";
            // 
            // textBox_trials
            // 
            this.textBox_trials.Location = new System.Drawing.Point(262, 371);
            this.textBox_trials.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_trials.Name = "textBox_trials";
            this.textBox_trials.Size = new System.Drawing.Size(124, 23);
            this.textBox_trials.TabIndex = 14;
            this.textBox_trials.TextChanged += new System.EventHandler(this.textBox_trials_TextChanged);
            // 
            // errorProvider_trials
            // 
            this.errorProvider_trials.ContainerControl = this;
            // 
            // radioButton_EuropeanCall
            // 
            this.radioButton_EuropeanCall.AutoSize = true;
            this.radioButton_EuropeanCall.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_EuropeanCall.Location = new System.Drawing.Point(33, 549);
            this.radioButton_EuropeanCall.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton_EuropeanCall.Name = "radioButton_EuropeanCall";
            this.radioButton_EuropeanCall.Size = new System.Drawing.Size(175, 20);
            this.radioButton_EuropeanCall.TabIndex = 15;
            this.radioButton_EuropeanCall.TabStop = true;
            this.radioButton_EuropeanCall.Text = "European Call Option";
            this.radioButton_EuropeanCall.UseVisualStyleBackColor = true;
            // 
            // radioButton_EuropeanPut
            // 
            this.radioButton_EuropeanPut.AutoSize = true;
            this.radioButton_EuropeanPut.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_EuropeanPut.Location = new System.Drawing.Point(262, 549);
            this.radioButton_EuropeanPut.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton_EuropeanPut.Name = "radioButton_EuropeanPut";
            this.radioButton_EuropeanPut.Size = new System.Drawing.Size(172, 20);
            this.radioButton_EuropeanPut.TabIndex = 17;
            this.radioButton_EuropeanPut.TabStop = true;
            this.radioButton_EuropeanPut.Text = "European Put Option";
            this.radioButton_EuropeanPut.UseVisualStyleBackColor = true;
            // 
            // label_OptionResult
            // 
            this.label_OptionResult.AutoSize = true;
            this.label_OptionResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_OptionResult.Location = new System.Drawing.Point(173, 19);
            this.label_OptionResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_OptionResult.Name = "label_OptionResult";
            this.label_OptionResult.Size = new System.Drawing.Size(95, 18);
            this.label_OptionResult.TabIndex = 20;
            this.label_OptionResult.Text = "Option Price";
            // 
            // label_Delta
            // 
            this.label_Delta.AutoSize = true;
            this.label_Delta.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Delta.Location = new System.Drawing.Point(27, 74);
            this.label_Delta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Delta.Name = "label_Delta";
            this.label_Delta.Size = new System.Drawing.Size(92, 18);
            this.label_Delta.TabIndex = 21;
            this.label_Delta.Text = "Delta Value:";
            // 
            // label_DeltaResult
            // 
            this.label_DeltaResult.AutoSize = true;
            this.label_DeltaResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_DeltaResult.Location = new System.Drawing.Point(173, 73);
            this.label_DeltaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_DeltaResult.Name = "label_DeltaResult";
            this.label_DeltaResult.Size = new System.Drawing.Size(88, 18);
            this.label_DeltaResult.TabIndex = 22;
            this.label_DeltaResult.Text = "Delta Value";
            // 
            // label_Gamma
            // 
            this.label_Gamma.AutoSize = true;
            this.label_Gamma.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Gamma.Location = new System.Drawing.Point(8, 128);
            this.label_Gamma.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Gamma.Name = "label_Gamma";
            this.label_Gamma.Size = new System.Drawing.Size(111, 18);
            this.label_Gamma.TabIndex = 23;
            this.label_Gamma.Text = "Gamma Value:";
            // 
            // label_GammaResult
            // 
            this.label_GammaResult.AutoSize = true;
            this.label_GammaResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_GammaResult.Location = new System.Drawing.Point(173, 127);
            this.label_GammaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_GammaResult.Name = "label_GammaResult";
            this.label_GammaResult.Size = new System.Drawing.Size(107, 18);
            this.label_GammaResult.TabIndex = 24;
            this.label_GammaResult.Text = "Gamma Value";
            // 
            // label_Vega
            // 
            this.label_Vega.AutoSize = true;
            this.label_Vega.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Vega.Location = new System.Drawing.Point(27, 183);
            this.label_Vega.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Vega.Name = "label_Vega";
            this.label_Vega.Size = new System.Drawing.Size(92, 18);
            this.label_Vega.TabIndex = 25;
            this.label_Vega.Text = "Vega Value:";
            // 
            // label_VegaResult
            // 
            this.label_VegaResult.AutoSize = true;
            this.label_VegaResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_VegaResult.Location = new System.Drawing.Point(173, 181);
            this.label_VegaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_VegaResult.Name = "label_VegaResult";
            this.label_VegaResult.Size = new System.Drawing.Size(88, 18);
            this.label_VegaResult.TabIndex = 26;
            this.label_VegaResult.Text = "Vega Value";
            // 
            // label_Theta
            // 
            this.label_Theta.AutoSize = true;
            this.label_Theta.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Theta.Location = new System.Drawing.Point(25, 235);
            this.label_Theta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Theta.Name = "label_Theta";
            this.label_Theta.Size = new System.Drawing.Size(94, 18);
            this.label_Theta.TabIndex = 27;
            this.label_Theta.Text = "Theta Value:";
            // 
            // label_ThetaResult
            // 
            this.label_ThetaResult.AutoSize = true;
            this.label_ThetaResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ThetaResult.Location = new System.Drawing.Point(173, 235);
            this.label_ThetaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_ThetaResult.Name = "label_ThetaResult";
            this.label_ThetaResult.Size = new System.Drawing.Size(90, 18);
            this.label_ThetaResult.TabIndex = 28;
            this.label_ThetaResult.Text = "Theta Value";
            // 
            // label_Rho
            // 
            this.label_Rho.AutoSize = true;
            this.label_Rho.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Rho.Location = new System.Drawing.Point(36, 289);
            this.label_Rho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Rho.Name = "label_Rho";
            this.label_Rho.Size = new System.Drawing.Size(83, 18);
            this.label_Rho.TabIndex = 29;
            this.label_Rho.Text = "Rho Value:";
            // 
            // label_RhoResult
            // 
            this.label_RhoResult.AutoSize = true;
            this.label_RhoResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RhoResult.Location = new System.Drawing.Point(173, 289);
            this.label_RhoResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_RhoResult.Name = "label_RhoResult";
            this.label_RhoResult.Size = new System.Drawing.Size(79, 18);
            this.label_RhoResult.TabIndex = 30;
            this.label_RhoResult.Text = "Rho Value";
            // 
            // label_SE
            // 
            this.label_SE.AutoSize = true;
            this.label_SE.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SE.Location = new System.Drawing.Point(42, 344);
            this.label_SE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_SE.Name = "label_SE";
            this.label_SE.Size = new System.Drawing.Size(77, 18);
            this.label_SE.TabIndex = 31;
            this.label_SE.Text = "SE Value:";
            // 
            // label_SEResult
            // 
            this.label_SEResult.AutoSize = true;
            this.label_SEResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SEResult.Location = new System.Drawing.Point(173, 343);
            this.label_SEResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_SEResult.Name = "label_SEResult";
            this.label_SEResult.Size = new System.Drawing.Size(73, 18);
            this.label_SEResult.TabIndex = 32;
            this.label_SEResult.Text = "SE Value";
            // 
            // checkBox_Antithetic
            // 
            this.checkBox_Antithetic.AutoSize = true;
            this.checkBox_Antithetic.Location = new System.Drawing.Point(387, 456);
            this.checkBox_Antithetic.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_Antithetic.Name = "checkBox_Antithetic";
            this.checkBox_Antithetic.Size = new System.Drawing.Size(98, 20);
            this.checkBox_Antithetic.TabIndex = 33;
            this.checkBox_Antithetic.Text = "Antithetic ";
            this.checkBox_Antithetic.UseVisualStyleBackColor = true;
            // 
            // label_VarianceReduction
            // 
            this.label_VarianceReduction.AutoSize = true;
            this.label_VarianceReduction.Location = new System.Drawing.Point(29, 456);
            this.label_VarianceReduction.Name = "label_VarianceReduction";
            this.label_VarianceReduction.Size = new System.Drawing.Size(148, 16);
            this.label_VarianceReduction.TabIndex = 34;
            this.label_VarianceReduction.Text = "Variance Reduction:";
            // 
            // checkBox_CV
            // 
            this.checkBox_CV.AutoSize = true;
            this.checkBox_CV.Location = new System.Drawing.Point(214, 456);
            this.checkBox_CV.Name = "checkBox_CV";
            this.checkBox_CV.Size = new System.Drawing.Size(131, 20);
            this.checkBox_CV.TabIndex = 35;
            this.checkBox_CV.Tag = "";
            this.checkBox_CV.Text = "Control Variate";
            this.checkBox_CV.UseVisualStyleBackColor = true;
            // 
            // label_Time
            // 
            this.label_Time.AutoSize = true;
            this.label_Time.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Time.Location = new System.Drawing.Point(23, 398);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(96, 18);
            this.label_Time.TabIndex = 36;
            this.label_Time.Text = "Time Usage:";
            // 
            // label_TimeResult
            // 
            this.label_TimeResult.AutoSize = true;
            this.label_TimeResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_TimeResult.Location = new System.Drawing.Point(173, 397);
            this.label_TimeResult.Name = "label_TimeResult";
            this.label_TimeResult.Size = new System.Drawing.Size(85, 18);
            this.label_TimeResult.TabIndex = 37;
            this.label_TimeResult.Text = "Time Value";
            // 
            // checkBox_MT
            // 
            this.checkBox_MT.AutoSize = true;
            this.checkBox_MT.Location = new System.Drawing.Point(214, 496);
            this.checkBox_MT.Name = "checkBox_MT";
            this.checkBox_MT.Size = new System.Drawing.Size(138, 20);
            this.checkBox_MT.TabIndex = 38;
            this.checkBox_MT.Text = "Multi-Threading";
            this.checkBox_MT.UseVisualStyleBackColor = true;
            // 
            // progressBar_Calculation
            // 
            this.progressBar_Calculation.Location = new System.Drawing.Point(32, 599);
            this.progressBar_Calculation.Maximum = 8;
            this.progressBar_Calculation.Name = "progressBar_Calculation";
            this.progressBar_Calculation.Size = new System.Drawing.Size(448, 23);
            this.progressBar_Calculation.TabIndex = 39;
            // 
            // label_Cores
            // 
            this.label_Cores.AutoSize = true;
            this.label_Cores.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Cores.Location = new System.Drawing.Point(38, 450);
            this.label_Cores.Name = "label_Cores";
            this.label_Cores.Size = new System.Drawing.Size(81, 18);
            this.label_Cores.TabIndex = 40;
            this.label_Cores.Text = "# of cores:";
            // 
            // label_CoresResult
            // 
            this.label_CoresResult.AutoSize = true;
            this.label_CoresResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CoresResult.Location = new System.Drawing.Point(173, 451);
            this.label_CoresResult.Name = "label_CoresResult";
            this.label_CoresResult.Size = new System.Drawing.Size(98, 18);
            this.label_CoresResult.TabIndex = 41;
            this.label_CoresResult.Text = "Cores Result";
            // 
            // groupBox
            // 
            this.groupBox.AutoSize = true;
            this.groupBox.Controls.Add(this.label_TimeResult);
            this.groupBox.Controls.Add(this.label_Cores);
            this.groupBox.Controls.Add(this.label_CoresResult);
            this.groupBox.Controls.Add(this.label_Time);
            this.groupBox.Controls.Add(this.label_Vega);
            this.groupBox.Controls.Add(this.label_SE);
            this.groupBox.Controls.Add(this.label_SEResult);
            this.groupBox.Controls.Add(this.label_Rho);
            this.groupBox.Controls.Add(this.label_RhoResult);
            this.groupBox.Controls.Add(this.label_Theta);
            this.groupBox.Controls.Add(this.label_ThetaResult);
            this.groupBox.Controls.Add(this.label_VegaResult);
            this.groupBox.Controls.Add(this.label_GammaResult);
            this.groupBox.Controls.Add(this.label_Gamma);
            this.groupBox.Controls.Add(this.label_optionPrice);
            this.groupBox.Controls.Add(this.label_DeltaResult);
            this.groupBox.Controls.Add(this.label_Delta);
            this.groupBox.Controls.Add(this.label_OptionResult);
            this.groupBox.Location = new System.Drawing.Point(568, 12);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(293, 527);
            this.groupBox.TabIndex = 42;
            this.groupBox.TabStop = false;
            // 
            // label_optionPrice
            // 
            this.label_optionPrice.AllowDrop = true;
            this.label_optionPrice.AutoSize = true;
            this.label_optionPrice.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_optionPrice.Location = new System.Drawing.Point(16, 20);
            this.label_optionPrice.Name = "label_optionPrice";
            this.label_optionPrice.Size = new System.Drawing.Size(103, 18);
            this.label_optionPrice.TabIndex = 42;
            this.label_optionPrice.Text = "Option Price: ";
            this.label_optionPrice.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 667);
            this.Controls.Add(this.progressBar_Calculation);
            this.Controls.Add(this.checkBox_MT);
            this.Controls.Add(this.checkBox_CV);
            this.Controls.Add(this.label_VarianceReduction);
            this.Controls.Add(this.checkBox_Antithetic);
            this.Controls.Add(this.radioButton_EuropeanPut);
            this.Controls.Add(this.radioButton_EuropeanCall);
            this.Controls.Add(this.textBox_trials);
            this.Controls.Add(this.label_trials);
            this.Controls.Add(this.label_N);
            this.Controls.Add(this.textBox_N);
            this.Controls.Add(this.textBox_sigma);
            this.Controls.Add(this.label_sigma);
            this.Controls.Add(this.textBox_T);
            this.Controls.Add(this.label_T);
            this.Controls.Add(this.textBox_r);
            this.Controls.Add(this.label_r);
            this.Controls.Add(this.label_K);
            this.Controls.Add(this.btn_calculate);
            this.Controls.Add(this.textBox_K);
            this.Controls.Add(this.label_S);
            this.Controls.Add(this.textBox_S);
            this.Controls.Add(this.groupBox);
            this.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Optoin Price Calculator 1.0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_r)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_T)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_sigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_trials)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_S;
        private System.Windows.Forms.Label label_S;
        private System.Windows.Forms.TextBox textBox_K;
        private System.Windows.Forms.Button btn_calculate;
        private System.Windows.Forms.ErrorProvider errorProvider_S;
        private System.Windows.Forms.Label label_K;
        private System.Windows.Forms.ErrorProvider errorProvider_K;
        private System.Windows.Forms.Label label_r;
        private System.Windows.Forms.TextBox textBox_r;
        private System.Windows.Forms.ErrorProvider errorProvider_r;
        private System.Windows.Forms.TextBox textBox_T;
        private System.Windows.Forms.Label label_T;
        private System.Windows.Forms.ErrorProvider errorProvider_T;
        private System.Windows.Forms.TextBox textBox_sigma;
        private System.Windows.Forms.Label label_sigma;
        private System.Windows.Forms.ErrorProvider errorProvider_sigma;
        private System.Windows.Forms.Label label_N;
        private System.Windows.Forms.TextBox textBox_N;
        private System.Windows.Forms.ErrorProvider errorProvider_N;
        private System.Windows.Forms.TextBox textBox_trials;
        private System.Windows.Forms.Label label_trials;
        private System.Windows.Forms.ErrorProvider errorProvider_trials;
        private System.Windows.Forms.RadioButton radioButton_EuropeanPut;
        private System.Windows.Forms.RadioButton radioButton_EuropeanCall;
        private System.Windows.Forms.Label label_OptionResult;
        private System.Windows.Forms.Label label_DeltaResult;
        private System.Windows.Forms.Label label_Delta;
        private System.Windows.Forms.Label label_GammaResult;
        private System.Windows.Forms.Label label_Gamma;
        private System.Windows.Forms.Label label_VegaResult;
        private System.Windows.Forms.Label label_Vega;
        private System.Windows.Forms.Label label_ThetaResult;
        private System.Windows.Forms.Label label_Theta;
        private System.Windows.Forms.Label label_RhoResult;
        private System.Windows.Forms.Label label_Rho;
        private System.Windows.Forms.Label label_SEResult;
        private System.Windows.Forms.Label label_SE;
        private System.Windows.Forms.CheckBox checkBox_Antithetic;
        private System.Windows.Forms.Label label_VarianceReduction;
        private System.Windows.Forms.CheckBox checkBox_CV;
        private System.Windows.Forms.Label label_TimeResult;
        private System.Windows.Forms.Label label_Time;
        private System.Windows.Forms.CheckBox checkBox_MT;
        private System.Windows.Forms.Label label_CoresResult;
        private System.Windows.Forms.Label label_Cores;
        public System.Windows.Forms.ProgressBar progressBar_Calculation;
        public System.Windows.Forms.GroupBox groupBox;
        public System.Windows.Forms.Label label_optionPrice;
    }
}

