﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

/// <summary>
/// This class is about GUI, inputs and outputs
/// </summary>
namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        // The error providers for inputs.
        // When user input a wrong type of data, the program should ask user to enter a right one
        private void Form1_Load(object sender, EventArgs e)
        {
            // Set some default value for convenient
            textBox_S.Text = "50";
            textBox_K.Text = "50";
            textBox_r.Text = "0.05";
            textBox_T.Text = "1";
            textBox_sigma.Text = "0.5";
            textBox_N.Text = "2";
            textBox_trials.Text = "10000";

            // Check how many cores in this computer
            int numOfThread = System.Environment.ProcessorCount;
            label_CoresResult.Text = Convert.ToString(numOfThread);

            // The error provider for underlying price text box
            errorProvider_S.SetIconAlignment(textBox_S, ErrorIconAlignment.MiddleRight);
            errorProvider_S.SetIconPadding(textBox_S, 3);

            // The error provider for strike price text box
            errorProvider_K.SetIconAlignment(textBox_K, ErrorIconAlignment.MiddleRight);
            errorProvider_K.SetIconPadding(textBox_K, 3);

            // The error provider for risk free rate text box
            errorProvider_r.SetIconAlignment(textBox_r, ErrorIconAlignment.MiddleRight);
            errorProvider_r.SetIconPadding(textBox_r, 3);

            // The error provider for tenor text box
            errorProvider_T.SetIconAlignment(textBox_T, ErrorIconAlignment.MiddleRight);
            errorProvider_T.SetIconPadding(textBox_T, 3);

            // The error provider for volatility text box
            errorProvider_sigma.SetIconAlignment(textBox_sigma, ErrorIconAlignment.MiddleRight);
            errorProvider_sigma.SetIconPadding(textBox_sigma, 3);

            // The error provider for steps text box
            errorProvider_N.SetIconAlignment(textBox_N, ErrorIconAlignment.MiddleRight);
            errorProvider_N.SetIconPadding(textBox_N, 3);

            // The error provider for trials text box
            errorProvider_trials.SetIconAlignment(textBox_trials, ErrorIconAlignment.MiddleRight);
            errorProvider_trials.SetIconPadding(textBox_trials, 3);

            // Clear the progress bar
            progressBar_Calculation.Value = 0;
        }

        // The sentence that ask user to enter correct type of data for underlying price
        private void textBox_S_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_S.Text, out num))
            {
                errorProvider_S.SetError(textBox_S, "Please enter a number");
            }
            else
            {
                errorProvider_S.SetError(textBox_S, "");
            }
            
        }

        // The sentence that ask user to enter correct type of data for strike price
        private void textBox_K_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_K.Text, out num))
            {
                errorProvider_K.SetError(textBox_K, "Please enter a number");
            }
            else
            {
                errorProvider_K.SetError(textBox_K, "");
            }
        }

        // The sentence that ask user to enter correct type of data for risk free rate
        private void textBox_r_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_r.Text, out num))
            {
                errorProvider_r.SetError(textBox_r, "Please enter a number");
            }
            else
            {
                errorProvider_r.SetError(textBox_r, "");
            }
        }

        // The sentence that ask user to enter correct type of data for tenor
        private void textBox_T_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_r.Text, out num))
            {
                errorProvider_T.SetError(textBox_T, "Please enter a number");
            }
            else
            {
                errorProvider_T.SetError(textBox_T, "");
            }
        }

        // The sentence that ask user to enter correct type of data for volatility
        private void textBox_sigma_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_sigma.Text, out num))
            {
                errorProvider_sigma.SetError(textBox_sigma, "Please enter a number");
            }
            else
            {
                errorProvider_sigma.SetError(textBox_sigma, "");
            }
        }

        // The sentence that ask user to enter correct type of data for steps
        private void textBox_N_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_N.Text, out num))
            {
                errorProvider_N.SetError(textBox_N, "Please enter a number");
            }
            else
            {
                errorProvider_N.SetError(textBox_N, "");
            }
        }

        // The sentence that ask user to enter correct type of data for trials
        private void textBox_trials_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_trials.Text, out num))
            {
                errorProvider_trials.SetError(textBox_trials, "Please enter a number");
            }
            else
            {
                errorProvider_trials.SetError(textBox_trials, "");
            }
        }


        private void btn_calculate_Click(object sender, EventArgs e)
        {

            // Clear the progress bar
            progressBar_Calculation.Value = 0;
            EuroOption opt = new EuroOption();

            if (double.TryParse(textBox_S.Text, out IO.S0) &&
                double.TryParse(textBox_K.Text, out IO.K) &&
                double.TryParse(textBox_r.Text, out IO.r) &&
                double.TryParse(textBox_T.Text, out IO.T) &&
                double.TryParse(textBox_sigma.Text, out IO.sigma) &&
                int.TryParse(textBox_N.Text, out IO.N) &&
                int.TryParse(textBox_trials.Text, out IO.trials))
            {

                // Check whether user want to calculate call option or put option
                if (radioButton_EuropeanPut.Checked)
                {
                    IO.isput = true;
                }
                else if (radioButton_EuropeanCall.Checked)
                {
                    IO.isput = false;
                }

                // Check whether user want to use control variate to reduce variance or not
                if (checkBox_CV.Checked)
                {
                    IO.useCV = true;
                }
                else
                {
                    IO.useCV = false;
                }

                // Check whether user want to use antithetic method or not
                if (checkBox_Antithetic.Checked)
                {
                    IO.isAnti = true;
                }
                else
                {
                    IO.isAnti = false;
                }

                progressBar_Calculation.Value = 0;

                // Check whether user want to use multi-threading
                if (checkBox_MT.Checked)
                {
                    IO.numOfThread = System.Environment.ProcessorCount;
                }
                else
                {
                    IO.numOfThread = 1;
                }

                Thread C = new Thread(new ThreadStart(opt.scheduler));
                C.Start();
            }
            // Output the error message if the user inputs inpropriate data
            else
            {
                label_OptionResult.Text = "Error! Please check your inputs";
            }

        }

        // The function used to output the result
        public void finish()
        {
            label_OptionResult.Text = IO.myPrice.ToString();
            label_DeltaResult.Text = IO.myDelta.ToString();
            label_GammaResult.Text = IO.myGamma.ToString();
            label_VegaResult.Text = IO.myVega.ToString();
            label_ThetaResult.Text = IO.myTheta.ToString();
            label_RhoResult.Text = IO.myRho.ToString();
            label_SEResult.Text = IO.mySE.ToString();
            label_CoresResult.Text = IO.numOfThread.ToString();
            label_TimeResult.Text = IO.myTime.ToString();

        }
    }
}
